#ifndef LIST_H
#define LIST_H
//make the node can store any type
template <class T>
class Node 
{
	public:
			T info; 
			Node<T> *next, *prev;
			Node() { next = prev = 0; }
			Node(T el, Node *n = 0, Node *p = 0) { info = el; next = n; prev = p; }	
};

//make the list can store any type
template <class T>
class List 
{
	public:
			List(){ head = tail = 0; }
			~List() 
			{
				for (Node<T> *p; !isEmpty(); ) 
				{
					p = head->next;
					delete head;
					head = p;
				}
			}

			int isEmpty() { return head == 0; }

			void headPush(T el)
			{
				Node<T> *tmp = new Node<T>(el); //declare new node
				if (head == NULL) //if not have any in list
				{
					head = tmp;
					tail = head;
				}
				else
				{
					//doubly link
					tmp->next = head;
					head->prev = tmp;
					head = tmp;
				}
			}

			void tailPush(T el)
			{
				Node<T> *tmp = new Node<T>(el);
				if (tail == NULL) //if not have any in list
				{
					tail = tmp;
					head = tail;
				}
				else
				{
					//doubly link
					tail->next = tmp;
					tmp->prev = tail;
					tail = tmp;
				}
			}

			void tailPop()
			{
				Node<T> *tmp = new Node<T>(NULL); //declare new node
				T value; //value that store info tail
				if (tail->prev == 0) //if only one in the list
				{
					tmp = tail;
					value = tmp->info;
				}
				else 
				{
					tmp = tail;
					tail = tail->prev;
					value = tmp->info;//store value that now pointing
				}
				delete tmp; //delete element that pointing
				return value; // return value that pointing before deleted
			}

			T headPop()
			{
				Node<T> *tmp = new Node<T>(NULL); //declare new node
				T value; //value that store info tail
				if (head->next == 0) //if only one in the list
				{
					tmp = head;
					value = tmp->info;
				}
				else 
				{
					tmp = head;
					head = head->next;
					value = tmp->info; //store value that now pointing
				}
				delete tmp; //delete element that pointing
				return value; // return value that pointing before deleted
			}

			void showEl() //show elements
			{
				Node<T> *tmp = head;
				if (head == NULL)
					cout << "Empty!!." << endl;
				else 
				{
					while (tmp != NULL)
					{
						cout << tmp->info << " ";
						tmp = tmp->next;
					}
				}
			}

			//Sorted any type
			void sorted()
			{
				Node<T> * tmp = head;
				T value; //Making value can store any type
				int counter = 0; //loop counter for run loop equal to number of lists
				
				while (tmp) //Count the list
				{
					tmp = tmp->next;
					counter++;
				}
				tmp = head; //Make tmp point to old position 
				//Adapt a Bubblesort to sorted value
				for (int j = 0; j<counter; j++)
				{
					while (tmp->next)
					{
						if (tmp->info > tmp->next->info) //swap value
						{
							value = tmp->info;
							tmp->info = tmp->next->info;
							tmp->next->info = value;
						}
						else
							tmp = tmp->next;
					}
					tmp = head; //Make tmp point to old position 
				}
			}

			//Remove duplicate from list
			void removeDup()
			{
				Node<T>* now = head;
				Node<T>* tmp;
				while (now->next != NULL)
				{
					if (now->info == now->next->info) //If value are same
					{
						tmp = now->next->next;
						delete now->next; //Delete node 
						now->next = tmp; 
					}
					else 
						now = now->next; //move to next list
				}
			}

			void deleteNode(T el)
			{
				Node<T> *tmp = head; //make new node point to head
				Node<T> *tmp2 = head;
				if (head == NULL)
				{
					head = head->next;
				}
				else
				{
					if (head == tail) //if element remain 1 do this
					{
						if (tmp->info == el)
						{
							head = NULL; //make head and tail point to NULL
							tail = NULL;//
							delete tmp; //delete element
						}
					}
					else
					{	
						if (tail->info == el) //if element at tail equal to element that users need to delete
						{
							tmp2 = tail;
							tail = tail->prev; //doubly link
							tail->next = NULL;
							delete tmp2;
						}
						else
						{
							if (head->info == el)  //if element at head equal to element that users need to delete
							{
								head = head->next; //make head point next position
								delete tmp;
							}
							else
							{
								if (tmp->next->next != NULL) 
								{ //to delete element between of head and tail
									while (tmp->next->next != NULL)
									{
										if (tmp->next->info == el)
										{
											//doubly link skip the list that will be delete
											tmp2 = tmp->next;
											tmp->next = tmp2->next;
											delete tmp2;
										}
										else
											tmp = tmp->next;
									}
								}
							}
						}
					}
				}
			}
private:
	Node<T> * head, *tail;
};



#endif //LIST