#include <iostream>
#include <string>
#include "list.h"
using namespace std;

template <class T>
void swap_values(T& x, T& y);

template <class T>
int index_of_smallest(const T a[], int start_index, int number_used);

template <class T>
void sort(T a[], int number_used);

template <class T>
void show(T a[], int number_used);

int main()
{	
	//Sorting array
	cout << "Numbers before sorted : ";
	int number[10] = { 0,2,1,5,3,4,6,9,7,8 };
	show(number, 10);
	sort(number, 10);
	cout << "\nNumbers after sorted : ";
	show(number, 10);
	cout << endl;
	
	cout << "\nAlphabets before sorted : ";
	char chararray[10] = { 'a', 'd', 'e', 'b', 'c', 'f', 'g', 'i', 'j', 'h' };
	show(chararray, 10);
	sort(chararray, 10);
	cout << "\nAlphabets after sorted : ";
	show(chararray, 10);
	cout << endl;
	
	cout << "\nDecimal numbers before sorted : ";
	double value[10] = { 0,0.1,0.3,0.2,0.01,1.01,2.3,5.1,4.22,3.14 };
	show(value, 10);
	sort(value, 10);
	cout << "\nDecimal numbers before sorted : ";
	show(value, 10);
	cout << endl;
	
	cout << "\nWords before sorted : ";
	string stringarray[10] = { "apple","banana" ,"coconut" , "pineapple" , "mango" , "grape" , "strawberry" , "blueberry" ,"avocado" , "watermelon" };
	show(stringarray, 10);
	sort(stringarray, 10);
	cout << "\nWords after sorted : ";
	show(stringarray, 10);
	cout << endl;

	//Sorting list
	cout << "\nList numbers before sorted : ";
	List <int> num; 
	num.tailPush(1); num.tailPush(3); num.tailPush(1); num.tailPush(1); num.tailPush(7); num.tailPush(7); num.tailPush(4); num.tailPush(5); num.tailPush(6);
	num.showEl();
	cout << "\nList numbers after sorted : ";
	num.sorted();
	num.showEl();
	cout << "\nList numbers after remove duplicates : ";
	num.removeDup();
	num.showEl();
	cout << endl;
	
	cout << "\nList alphabets before sorted : ";
	List <char> alpha;
	alpha.tailPush('a'); alpha.tailPush('b'); alpha.tailPush('a'); alpha.tailPush('b'); alpha.tailPush('a'); alpha.tailPush('b'); alpha.tailPush('d'); alpha.tailPush('c'); alpha.tailPush('e');
	alpha.showEl();
	cout << "\nList alphabets after sorted : ";
	alpha.sorted();
	alpha.showEl();
	cout << "\nList alphabets after remove duplicates : ";
	alpha.removeDup();
	alpha.showEl();
	cout << endl;

	cout << "\nList decimals before sorted : ";
	List <double> decimal;
	decimal.tailPush(1.01); decimal.tailPush(0.01); decimal.tailPush(0.01); decimal.tailPush(1.01); decimal.tailPush(10.01); decimal.tailPush(7.23); decimal.tailPush(4.00); decimal.tailPush(3.68); decimal.tailPush(1.00);
	decimal.showEl();
	cout << "\nList decimals after sorted : ";
	decimal.sorted();
	decimal.showEl();
	cout << "\nList decimals after remove duplicates : ";
	decimal.removeDup();
	decimal.showEl();
	cout << endl;

	cout << "\nList words before sorted : ";
	List <string> word;
	word.tailPush("Hello"); word.tailPush("Ken"); word.tailPush("Hello"); word.tailPush("Same"); word.tailPush("GiveA"); word.tailPush("Same"); word.tailPush("Hello"); word.tailPush("Please"); word.tailPush("Thankyou");
	word.showEl();
	cout << "\nList words after sorted : ";
	word.sorted();
	word.showEl();
	cout << "\nList words after remove duplicates : ";
	word.removeDup();
	word.showEl();
	cout << endl;
	system("pause");
}

//Swap values
template <class T>
void swap_values(T& x, T& y)
{
	T tmp = x;
	x = y;
	y = tmp;
}

//Find the value of smallest
template <class T>
int index_of_smallest(const T a[], int start_index, int number_used)
{
	T min = a[start_index];
	int index_of_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
		if (a[index] < min)
		{
			min = a[index];
			index_of_min = index;
		}
	return index_of_min;
}

//Sorted the array
template <class T>
void sort(T a[], int number_used)
{
	int index_of_next_smallest;
	for (int index = 0; index < number_used - 1; index++)
	{
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
}
//Show the value from array
template <class T>
void show(T a[], int number_used)
{
	for (int i = 0; i < number_used; i++)
		cout << a[i] << " ";
}

